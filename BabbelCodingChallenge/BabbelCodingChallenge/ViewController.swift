//
//  ViewController.swift
//  BabbelCodingChallenge
//
//  Created by Ubair Hamid on 10/3/19.
//  Copyright © 2019 Ubair. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    let gameViewController: GameViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "GameViewController") as! GameViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func englishToGermanPressed(_ sender: Any) {
        gameViewController.isEnglishToGerman = true
        self.present(gameViewController, animated: true, completion: nil)
    }
    
    @IBAction func germanToEnglishPressed(_ sender: Any) {
        gameViewController.isEnglishToGerman = false
        self.present(gameViewController, animated: true, completion: nil)
    }
}


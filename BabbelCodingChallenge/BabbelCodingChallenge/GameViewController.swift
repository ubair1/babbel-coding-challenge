//
//  GameViewController.swift
//  BabbelCodingChallenge
//
//  Created by Ubair Masood (DeG) on 10/3/19.
//  Copyright © 2019 Ubair. All rights reserved.
//

import UIKit

class GameViewController: UIViewController {
    var isEnglishToGerman: Bool = true
    
    @IBOutlet weak var fallingWordLabel: UILabel!
    
    @IBOutlet weak var correctCountLabel: UILabel!
    @IBOutlet weak var incorrectCountLabel: UILabel!
    @IBOutlet weak var actualWordLabel: UILabel!
    
    @IBOutlet weak var correctButton: UIButton!
    @IBOutlet weak var incorrectButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        animateFallingWordLabel()
    }
    
    func animateFallingWordLabel() {
        
    }
    
    @IBAction func correctPressed(_ sender: Any) {
    }
    
    
    @IBAction func incorrectPressed(_ sender: Any) {
    }
    

}
